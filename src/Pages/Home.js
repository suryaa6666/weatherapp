import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList } from 'react-native';
import axios from 'axios';
import { FaPercent, FaCompress, FaCloud, FaPlane, FaTemperatureHigh } from 'react-icons/fa';

export default function Home({ route, navigation }) {

  const [dataKota, setDataKota] = useState();
  const [dataKotaForecast, setDataKotaForecast] = useState();
  const [currentWeatherTime, setCurrentWeatherTime] = useState();
  const [isStarted, setIsStarted] = useState(false);

  let current = new Date();
  const greet = [
    {
      "status": "Pagi",
      "jam": 5,
      "sampai": 10
    },
    {
      "status": "Siang",
      "jam": 10,
      "sampai": 15
    },
    {
      "status": "Sore",
      "jam": 15,
      "sampai": 18
    },
    {
      "status": "Petang",
      "jam": 18,
      "sampai": 19
    },
    {
      "status": "Malam",
      "jam": 19,
      "sampai": 24
    },
    {
      "status": "Dini Hari",
      "jam": 0,
      "sampai": 3
    },
    {
      "status": "Subuh",
      "jam": 3,
      "sampai": 5
    },
  ]

  const getDataKota = async () => {
    const nama_kota = route.params.key.split(' ')[1];
    const api_key = 'debd18743a368466cc1d0dfe2dbe8b85';
    try {
      const response = await axios({
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'GET',
        url: `https://api.openweathermap.org/geo/1.0/direct?q=${nama_kota},ID&limit=1&appid=${api_key}`
      })
      setDataKota(response["data"][0]);
    } catch (error) {
      console.log("error : ", error);
    }
  }

  const getDataKotaForecast = async () => {
    const api_key = 'debd18743a368466cc1d0dfe2dbe8b85';
    let lat = await dataKota["lat"];
    let lon = await dataKota["lon"];
    try {
      const response = await axios({
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'GET',
        url: `https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=${api_key}`
      })
      setDataKotaForecast(response["data"]);
      response["data"]["list"].map((item, index) => {
        let zone = new Date().toLocaleTimeString('en-us', { timeZoneName: 'short' }).split(' ')[2];
        let time = zone.substring(4, zone.length);
        let timesymbol = zone.substring(3, zone.length);
        let currenttime;
        if (timesymbol[0] == "+") {
          currenttime = current.getTime() / 1000 + (time * 60 * 60);
        } else {
          currenttime = current.getTime() / 1000 - (time * 60 * 60);
        }
        if (currenttime >= item.dt && currenttime <= response["data"]["list"][index + 1].dt) {
          setCurrentWeatherTime(item);
        }
      })
      setIsStarted(true);
    } catch (error) {
      console.log("error : ", error);
    }
  }

  // const test = async () => {
  //   console.log("currentweather", currentWeatherTime);
  //   console.log("data forecast", dataKotaForecast);
  //   console.log("data kota : ", dataKota);
  // }

  useEffect(() => {
    getDataKota();
  }, [])


  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.submitBtn} onPress={() => { getDataKotaForecast() }}>
        <Text> {isStarted ? 'REFRESH' : 'START'} </Text>
      </TouchableOpacity>
      {/* <TouchableOpacity style={styles.submitBtn} onPress={() => { test() }}>
        <Text> DEBUG </Text>
      </TouchableOpacity> */}
      <Text> {isStarted ? dataKotaForecast["city"]["name"] : ''} </Text>
      <Text> {`${current.toLocaleDateString('default', { weekday: 'long' })}, ${current.toLocaleString('default', { month: 'long' })} ${current.getDate()}, ${current.getFullYear()}`} </Text>
      <View style={styles.greetcontainer}>
        <Text> Selamat {
          greet.map((item) => {
            if (current.getHours() >= item["jam"] && current.getHours() < item["sampai"]) {
              return item["status"];
            }
          })
        }, {route.params.name}
        </Text>
        <Text>
          {isStarted ? currentWeatherTime["weather"][0]["main"] : ''}
        </Text>
      </View>
      <View style={styles.container}>
        {/* {
          isStarted ? dataKotaForecast["list"].map((item, index) => {
            let tanggal = item["dt_txt"].split(' ')[0];
            let jamarray = item["dt_txt"].split(' ')[1].split(':');
            let jam = `${jamarray[0]}:${jamarray[1]}`;
            return (
              <View key={index}>
                <View style={styles.flexcol}>
                  <Text> {tanggal} </Text>
                  <Image source={{ uri: `http://openweathermap.org/img/wn/${item["weather"][0]["icon"]}@2x.png`, width: 50, height: 20 }} />
                  <Text>{jam}</Text>
                  <Text>{(item["main"]["temp"] / 10).toFixed(2)} °C</Text>
                </View>
              </View>
            )
          }) : <Text></Text>
        } */}
        {
          isStarted ? <FlatList
            data={dataKotaForecast["list"]}
            contentContainerStyle={{ flexGrow: 1 }}
            keyExtractor={(item, index) => index.toString()}
            numColumns={4}
            renderItem={(item) => {
              let tanggal = item.item["dt_txt"].split(' ')[0];
              let jamarray = item.item["dt_txt"].split(' ')[1].split(':');
              let jam = `${jamarray[0]}:${jamarray[1]}`;
              return (
                <View style={styles.flexcol}>
                  <Text> {tanggal} </Text>
                  <Image source={{ uri: `http://openweathermap.org/img/wn/${item.item["weather"][0]["icon"]}@2x.png`, width: 50, height: 20 }} />
                  <Text>{jam}</Text>
                  <Text>{(item.item["main"]["temp"] / 10).toFixed(2)} °C</Text>
                </View>
              );
            }}
          /> : <></>
        }
      </View>
      <Text style={styles.temp}>
        <FaTemperatureHigh />
        <Text>{isStarted ? (currentWeatherTime["main"]["temp"] / 10).toFixed(2) : ''} °C</Text>
      </Text>
      <View style={styles.infocontainer}>
        <View style={styles.flexcol}><FaPercent /><Text>{isStarted ? currentWeatherTime["main"]["humidity"] : ''}</Text><View><Text>Humidity</Text></View></View>
        <View style={styles.flexcol}><FaCompress /><Text>{isStarted ? currentWeatherTime["main"]["pressure"] : ''}</Text><View><Text>Pressure</Text></View></View>
        <View style={styles.flexcol}><FaCloud /><Text>{isStarted ? currentWeatherTime["clouds"]["all"] : ''}</Text><View><Text>Cloudliness</Text></View></View>
        <View style={styles.flexcol}><FaPlane /><Text>{isStarted ? currentWeatherTime["wind"]["speed"] : ''}</Text><View><Text>Wind</Text></View></View>
      </View>
    </View >
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#95E1D3',
    alignItems: 'center',
    justifyContent: 'center',
  },
  submitBtn: {
    flex: 1,
    width: '100%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FCE38A'
  },
  greetcontainer: {
    flex: 1,
    flexDirection: 'row'
  },
  temp: {
    flex: 1,
    fontSize: 24,
    color: 'white',
    fontWeight: 'bold'
  },
  infocontainer: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#EAFFD0',
    height: 50
  },
  flexcol: {
    flex: 1,
    flexDirection: 'column',
    padding: 15,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
  },
});
