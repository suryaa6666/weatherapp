import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, TextInput, Picker, TouchableOpacity } from 'react-native';
import axios from "axios";
import { useNetInfo } from "@react-native-community/netinfo";



export default function Welcome({ route, navigation }) {

  const [name, setName] = useState('');
  const [dataProvinsi, setDataProvinsi] = useState();
  const [dataKota, setDataKota] = useState();
  const [selectedValueProvinsi, setSelectedValueProvinsi] = useState("");
  const [selectedValueKota, setSelectedValueKota] = useState("");
  const [errorValidation, setErrorValidation] = useState("");
  const [canSubmit, setCanSubmit] = useState(false);

  const getDataProvinsi = async () => {
    try {
      const response = await axios({
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'GET',
        url: 'https://dev.farizdotid.com/api/daerahindonesia/provinsi'
      })
      setDataProvinsi(response["data"]["provinsi"]);
    } catch (error) {
      setErrorValidation("Tidak ada koneksi internet!");
      console.log("error : ", error);
    }
  }

  const getDataKota = async (nama_provinsi) => {
    let id_provinsi;
    await dataProvinsi.map((item) => {
      if (item["nama"] == nama_provinsi) {
        id_provinsi = item["id"];
        return;
      }
    })
    try {
      const response = await axios({
        headers: {
          'Content-Type': 'application/json'
        },
        method: 'GET',
        url: `https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${id_provinsi}`
      })
      setDataKota(response["data"]["kota_kabupaten"]);
    } catch (error) {
      setErrorValidation("Tidak ada koneksi internet!");
      console.log("error : ", error);
    }
  }

  useEffect(() => {
    getDataProvinsi();
  }, [])

  const netInfo = useNetInfo();

  const submit = () => {
    if (!netInfo.isConnected) {
      setCanSubmit(false);
    } else {
      setCanSubmit(true);
    }
    if (name == "" || selectedValueProvinsi == "" || selectedValueKota == "") {
      setErrorValidation("Data tidak boleh kosong!");
    } else {
      if (canSubmit)
        navigation.navigate("Home", { name: name, key: selectedValueKota })
    }
  }

  return (
    <View style={styles.container}>
      <Text style={styles.errorvalidation}>
        {errorValidation}
      </Text>
      <TextInput
        style={styles.nameinput}
        placeholder="Nama lengkap..."
        value={name}
        onChangeText={(value) => setName(value)}
      />
      <Picker
        selectedValue={selectedValueProvinsi}
        style={styles.picker}
        onValueChange={(itemValue, itemIndex) => { setSelectedValueProvinsi(itemValue); getDataKota(itemValue) }}
      >
        <Picker.Item label="Pilih Provinsi..." value="" />
        {
          dataProvinsi?.map((item, index) => {
            return (
              <Picker.Item label={item["nama"]} value={item["nama"]} key={index} />
            )
          })
        }
      </Picker>
      <Picker
        selectedValue={selectedValueKota}
        style={styles.picker}
        onValueChange={(itemValue, itemIndex) => setSelectedValueKota(itemValue)}
      >
        <Picker.Item label="Pilih Kabupaten / Kota..." value="" />
        {
          dataKota?.map((item, index) => {
            return (
              <Picker.Item label={item["nama"]} value={item["nama"]} key={index} />
            )
          })
        }
      </Picker>
      <TouchableOpacity style={styles.submitBtn} onPress={() => { submit() }}>
        <Text> GO! </Text>
      </TouchableOpacity>
    </View >
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#95E1D3',
    justifyContent: "center",
    alignItems: "center",
  },
  nameinput: {
    borderWidth: 1,
    paddingVertical: 10,
    borderRadius: 5,
    width: '80%',
    paddingHorizontal: 10,
    backgroundColor: 'white',
    marginTop: 10
  },
  picker: {
    height: 50,
    width: '80%',
    marginTop: 10
  },
  submitBtn: {
    marginTop: 10,
    width: '80%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FCE38A'
  },
  errorvalidation: {
    color: 'red',
    fontWeight: '800'
  }
});