import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import "react-native-gesture-handler";
import Welcome from "./Pages/Welcome";
import Home from "./Pages/Home";

const Stack = createNativeStackNavigator();

export default function index() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Welcome">
                <Stack.Screen
                    name="Welcome"
                    component={Welcome}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Home"
                    component={Home}
                    options={{ headerTitle: "Perkiraan Cuaca" }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
}
