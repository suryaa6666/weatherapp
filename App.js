import { StatusBar } from 'expo-status-bar';
import WeatherApp from './src/index';

export default function App() {
  return (
    <>
      <WeatherApp />
      <StatusBar translucent={false} />
    </>
  );
}
